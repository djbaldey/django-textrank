from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.urls import path, include

locale_urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('textrank.urls')),
]

urlpatterns = i18n_patterns(
    path('', include(locale_urlpatterns)),
)

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
