#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.core.exceptions import ValidationError
from django.db import transaction
from django.db.utils import DatabaseError
from django.test import TestCase
from django.utils.crypto import get_random_string

from textrank.models import Group, Keyword, Weight, Sample, Topic


class GroupTestCase(TestCase):

    def test_create(self):
        group = Group.objects.create(name='Группа 1')
        self.assertIsNotNone(group.created)
        self.assertIsNotNone(group.updated)
        self.assertEqual(group.name, 'Группа 1')
        self.assertIsNotNone(group.code)
        with transaction.atomic():
            with self.assertRaises(DatabaseError):
                group.pk = None
                group.save()

    def test_create_with_topic(self):
        topic = Topic.objects.create(name='Раздел 1')
        group = Group.objects.create(name='Группа 1', topic=topic)
        self.assertIsNotNone(group.created)
        self.assertIsNotNone(group.updated)
        self.assertEqual(group.name, 'Группа 1')
        self.assertEqual(group.topic, topic)
        self.assertIsNotNone(group.code)
        with transaction.atomic():
            with self.assertRaises(DatabaseError):
                group.pk = None
                group.save()

    def test_disable_delete(self):
        qs = Group.objects.all()
        group = qs.create(name='Группа')
        self.assertEqual(qs.count(), 1)
        self.assertTrue(group.is_active)
        group.delete()
        self.assertEqual(qs.count(), 1)
        self.assertFalse(group.is_active)


class KeywordTestCase(TestCase):

    def test_create(self):
        keyword = Keyword.objects.create(word='слово')
        self.assertIsNotNone(keyword.created)
        self.assertIsNotNone(keyword.updated)
        self.assertEqual(keyword.word, 'слово')
        with transaction.atomic():
            with self.assertRaises(DatabaseError):
                keyword.pk = None
                keyword.save()

    def test_create_duplicate(self):
        # Без раздела.
        Keyword.objects.create(word='слово')
        with transaction.atomic():
            with self.assertRaises(DatabaseError):
                Keyword.objects.create(word='слово')

    def test_create_duplicate_topic(self):
        # С разделом
        topic = Topic.objects.create(name='Раздел 1')
        Keyword.objects.create(word='слово', topic=topic)
        with transaction.atomic():
            with self.assertRaises(DatabaseError):
                Keyword.objects.create(word='слово', topic=topic)

    def test_validate_word(self):
        self.assertTrue(Keyword.validate_word('два'))
        self.assertTrue(Keyword.validate_word('слова'))
        self.assertTrue(Keyword.validate_word('два-слова'))
        self.assertTrue(Keyword.validate_word('два_слова'))
        self.assertTrue(Keyword.validate_word('два+слова'))
        self.assertFalse(Keyword.validate_word('два слова'))
        self.assertFalse(Keyword.validate_word('два,слова'))
        self.assertFalse(Keyword.validate_word('два;слова'))
        keyword = Keyword(word='два')
        keyword.save()
        keyword.word = 'слова'
        keyword.save()
        keyword.word = 'два-слова'
        keyword.save()
        keyword.word = 'два_слова'
        keyword.save()
        keyword.word = 'два+слова'
        keyword.save()
        with self.assertRaises(ValidationError):
            keyword.word = 'два слова'
            keyword.save()
        with self.assertRaises(ValidationError):
            keyword.word = 'два,слова'
            keyword.save()
        with self.assertRaises(ValidationError):
            keyword.word = 'два;слова'
            keyword.save()

    def test_disable_delete(self):
        qs = Keyword.objects.all()
        keyword = qs.create(word='слово')
        self.assertEqual(qs.count(), 1)
        self.assertTrue(keyword.is_active)
        keyword.delete()
        self.assertEqual(qs.count(), 1)
        self.assertFalse(keyword.is_active)


class WeightTestCase(TestCase):

    def test_create(self):
        group = Group.objects.create(name='Группа 1')
        keyword = Keyword.objects.create(word='слово')
        weight = Weight.objects.create(group=group, keyword=keyword)
        self.assertIsNotNone(weight.created)
        self.assertIsNotNone(weight.updated)
        self.assertEqual(weight.group, group)
        self.assertEqual(weight.keyword, keyword)
        self.assertEqual(weight.value, 1)
        with transaction.atomic():
            with self.assertRaises(DatabaseError):
                weight.pk = None
                weight.save()
        with transaction.atomic():
            with self.assertRaises(DatabaseError):
                weight.pk = None
                weight.value += 1
                weight.save()

    def test_is_active(self):
        group = Group.objects.create(name='Группа 1')
        keyword = Keyword.objects.create(word='слово')
        weight = Weight.objects.create(group=group, keyword=keyword)
        self.assertTrue(weight.is_active)
        group.is_active = False
        self.assertFalse(weight.is_active)
        group.is_active = True
        keyword.is_active = False
        self.assertFalse(weight.is_active)
        group.is_active = keyword.is_active = False
        self.assertFalse(weight.is_active)


class SampleTestCase(TestCase):

    def test_create(self):
        group1 = Group.objects.create(name='Группа 1')
        group2 = Group.objects.create(name='Группа 2')
        message = get_random_string(1024)
        sample = Sample.objects.create(group=group1, message=message)
        self.assertIsNotNone(sample.created)
        self.assertIsNotNone(sample.updated)
        self.assertEqual(sample.group, group1)
        self.assertEqual(sample.message, message)
        with transaction.atomic():
            with self.assertRaises(DatabaseError):
                sample.pk = None
                sample.save()
        # Free copying for another group.
        sample.pk = None
        sample.group = group2
        sample.save()


class TopicTestCase(TestCase):

    def test_create(self):
        topic = Topic.objects.create(name='Раздел 1')
        self.assertIsNotNone(topic.created)
        self.assertIsNotNone(topic.updated)
        self.assertEqual(topic.name, 'Раздел 1')
        self.assertIsNotNone(topic.code)
        with transaction.atomic():
            with self.assertRaises(DatabaseError):
                topic.pk = None
                topic.save()

    def test_disable_delete(self):
        qs = Topic.objects.all()
        topic = qs.create(name='Раздел 1')
        self.assertEqual(qs.count(), 1)
        self.assertTrue(topic.is_active)
        topic.delete()
        self.assertEqual(qs.count(), 1)
        self.assertFalse(topic.is_active)
