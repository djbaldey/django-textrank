#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.test import TestCase

from textrank import analyzer
from textrank.models import Group, Keyword, Weight


class DefaultTestCase(TestCase):

    def setUp(self):
        analyzer.GROUPS.clear()
        analyzer.INFO.clear()
        analyzer.DICTS.clear()

    def test_check_updates(self):
        utime = analyzer.check_updates()
        self.assertGreater(utime, 0)
        self.assertEqual(analyzer.GROUPS, {})
        self.assertEqual(analyzer.DICTS, {})
        self.assertIn('last_id', analyzer.INFO)
        self.assertIn('last_updated', analyzer.INFO)

    def test_analyze_blank_text(self):
        result = analyzer.analyze_text('')
        self.assertIn('topic', result)
        self.assertIn('group', result)
        self.assertIn('found', result)
        self.assertIn('words', result)
        self.assertIn('morph', result)
        self.assertIn('other', result)
        self.assertIn('utime', result)
        self.assertIn('atime', result)


class AnalyzeTestCase(TestCase):

    def setUp(self):
        analyzer.GROUPS.clear()
        analyzer.INFO.clear()
        analyzer.DICTS.clear()

        self.group1 = Group.objects.create(name='Сеть')
        self.group2 = Group.objects.create(name='Программы')
        self.group3 = Group.objects.create(name='Прочее')

        keyword1 = Keyword.objects.create(word='сеть')
        keyword2 = Keyword.objects.create(word='интернет')
        keyword3 = Keyword.objects.create(word='не_работает_интернет')
        keyword4 = Keyword.objects.create(word='сеть+упала')
        keyword5 = Keyword.objects.create(word='программа')
        keyword6 = Keyword.objects.create(word='установить')
        keyword7 = Keyword.objects.create(word='настроить')
        keyword8 = Keyword.objects.create(word='работа')

        Weight.objects.bulk_create([
            Weight(group=self.group1, keyword=keyword1),
            Weight(group=self.group1, keyword=keyword2),
            Weight(group=self.group1, keyword=keyword3, value=100),
            Weight(group=self.group1, keyword=keyword4, value=100),
            Weight(group=self.group1, keyword=keyword7),

            Weight(group=self.group2, keyword=keyword5),
            Weight(group=self.group2, keyword=keyword6),
            Weight(group=self.group2, keyword=keyword7),

            Weight(group=self.group3, keyword=keyword6),
            Weight(group=self.group3, keyword=keyword7),
            Weight(group=self.group3, keyword=keyword8),
        ])

    def test_analyze_text(self):
        result = analyzer.analyze_text('У меня упала сеть.')
        self.assertEqual(result['group']['id'], self.group1.id)

        result = analyzer.analyze_text('Сеть наверное упала. Прошу настроить.')
        self.assertEqual(result['group']['id'], self.group1.id)
        self.assertEqual(result['other'][0]['group']['id'], self.group2.id)
        self.assertIn('сеть+упала', result['found'])
        self.assertIn('настроить', result['found'])

        result = analyzer.analyze_text('У меня не работает чтот-то интернет.')
        self.assertEqual(result['group']['id'], self.group1.id)
        self.assertNotIn('не_работает_интернет', result['found'])

        result = analyzer.analyze_text('Упала походу сеть, нет интернета.')
        self.assertEqual(result['group']['id'], self.group1.id)
        self.assertNotIn('не_работает_интернет', result['found'])
        self.assertIn('сеть+упала', result['found'])
        self.assertIn('интернет', result['found'])

    def test_coverwords_with_repeated_words(self):
        """
        Повторяющиеся слова не должны увеличивать результат покрытия слов.
        """

        result = analyzer.analyze_text(
            'Сеть наверное упала. Прошу настроить.')
        self.assertIn('сеть+упала', result['found'])

        count = result['found']['сеть+упала']

        variants = (
            'Сеть наверное упала. Прошу настроить сеть.',
            'Сеть наверное упала. Прошу настроить сети.',
            'Сеть наверное упала. Невозможно пользоваться сетью.',
            'Прошу настроить сеть. Сеть наверное упала.',
            'Прошу настроить сети. Сеть наверное упала.',
            'Невозможно пользоваться сетью. Сеть наверное упала.',
        )

        for text in variants:
            result = analyzer.analyze_text(text)
            self.assertIn('сеть+упала', result['found'], text)
            self.assertEqual(count, result['found']['сеть+упала'], text)

    def test_coverwords_not_count_from_repeated_words(self):
        """
        Исключаем ошибку засчёта повторяющихся слов за покрытие.
        """
        variants = (
            'У нас есть сеть, другая компания смонтировала свою сеть рядом.',
            'У нас есть сеть, другая компания смонтировала свои сети рядом.',
            'Рядом с нашей сетью другая компания смонтировала свою сеть.',
            'Рядом с нашей сетью другая компания смонтировала свои сети.',
        )

        for text in variants:
            result = analyzer.analyze_text(text)
            self.assertNotIn('сеть+упала', result['found'], text)
            self.assertIn('сеть', result['found'], text)
            self.assertEqual(result['found']['сеть'], 2, text)

    def test_change_keywords(self):
        storages = analyzer.get_dictionaries(None)
        analyzer.analyze_text('Обновление.')

        for store in storages:
            self.assertEqual(len(store.keys()), len(store.words.keys()))

        keyword = Keyword.objects.get(word='сеть+упала')
        keyword.word = 'упала+сетка'
        keyword.save()
        analyzer.analyze_text('Обновление.')

        for store in storages:
            self.assertEqual(len(store.keys()), len(store.words.keys()))
