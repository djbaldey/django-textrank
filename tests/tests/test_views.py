#
# Copyright (c) 2019, Grigoriy Kramarenko
# All rights reserved.
# This file is distributed under the BSD 3-Clause License.
#
from django.contrib.auth import get_user_model
from django.shortcuts import resolve_url
from django.test import TestCase

from textrank.models import Group, Keyword, Weight, Sample, Topic

User = get_user_model()


class ViewsTestCase(TestCase):

    def test_index(self):
        url = resolve_url('textrank:index')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302, response.content)
        self.client.force_login(User.objects.create(username='user'))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        self.assertIn('all_groups', response.context)
        self.assertIn('last_groups', response.context)
        self.assertIn('active_groups', response.context)
        self.assertIn('notactive_groups', response.context)
        self.assertIn('all_keywords', response.context)
        self.assertIn('last_keywords', response.context)
        self.assertIn('active_keywords', response.context)
        self.assertIn('notactive_keywords', response.context)
        self.assertIn('last_weights', response.context)

    def test_add_group(self):
        qs = Group.objects.all()
        self.assertEqual(qs.count(), 0)
        url = resolve_url('textrank:groups')
        data = {'name': 'Группа', 'code': 'code'}
        response = self.client.post(url, data)
        self.assertIn(response.status_code, (302, 403), response.content)
        self.client.force_login(User.objects.create(username='user'))
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200, response.content)
        self.assertEqual(qs.count(), 1)
        newgroup = qs[0]
        self.assertEqual(newgroup.name, 'Группа')
        self.assertEqual(newgroup.code, 'code')

    def test_change_group(self):
        qs = Group.objects.all()
        self.assertEqual(qs.count(), 0)
        group = qs.create(name='Группа')
        self.assertEqual(qs.count(), 1)
        url = resolve_url('textrank:group', id=group.id)
        data = {'name': 'другое', 'code': 'code'}
        response = self.client.post(url, data)
        self.assertIn(response.status_code, (302, 403), response.content)
        self.client.force_login(User.objects.create(username='user'))
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200, response.content)
        newgroup = qs[0]
        self.assertEqual(newgroup.name, 'другое')
        self.assertEqual(newgroup.code, 'code')

    def test_delete_group(self):
        qs = Group.objects.all()
        self.assertEqual(qs.count(), 0)
        group = qs.create(name='Группа')
        self.assertEqual(qs.count(), 1)
        url = resolve_url('textrank:group', id=group.id)
        response = self.client.delete(url)
        self.assertIn(response.status_code, (302, 403), response.content)
        self.client.force_login(User.objects.create(username='user'))
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 200, response.content)
        self.assertEqual(qs.count(), 1)
        self.assertEqual(qs.filter(is_active=True).count(), 0)

    def test_add_keyword(self):
        qs = Keyword.objects.all()
        self.assertEqual(qs.count(), 0)
        url = resolve_url('textrank:keywords')
        data = {'word': 'слово'}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302, response.content)
        self.client.force_login(User.objects.create(username='user'))
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200, response.content)
        self.assertEqual(qs.count(), 1)
        self.assertEqual(qs[0].word, 'слово')

    def test_change_keyword(self):
        qs = Keyword.objects.all()
        self.assertEqual(qs.count(), 0)
        keyword = qs.create(word='keyword')
        self.assertEqual(qs.count(), 1)
        url = resolve_url('textrank:keyword', id=keyword.id)
        data = {'word': 'другое'}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302, response.content)
        self.client.force_login(User.objects.create(username='user'))
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200, response.content)
        self.assertEqual(qs[0].word, 'другое')

    def test_delete_keyword(self):
        qs = Keyword.objects.all()
        self.assertEqual(qs.count(), 0)
        keyword = qs.create(word='слово')
        self.assertEqual(qs.count(), 1)
        url = resolve_url('textrank:keyword', id=keyword.id)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 302, response.content)
        self.client.force_login(User.objects.create(username='user'))
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 200, response.content)
        self.assertEqual(qs.count(), 1)
        self.assertEqual(qs.filter(is_active=True).count(), 0)

    def test_analyze_for_html_format(self):
        Weight.objects.create(
            group=Group.objects.create(name='Группа'),
            keyword=Keyword.objects.create(word='слово'),
        )
        url = resolve_url('textrank:rank', format='html')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403, response.content)
        self.client.force_login(User.objects.create(username='user'))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200, response.content)
        data = {'text': 'Тестовый текст, включая слово.', 'verbose': 1}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200, response.content)
        self.assertTrue(response.context['form'].is_valid())
        self.assertIn('result', response.context)
        result = response.context['result']
        self.assertIn('group', result)
        self.assertIn('found', result)
        self.assertIn('words', result)
        self.assertIn('morph', result)
        self.assertIn('other', result)
        self.assertIn('utime', result)
        self.assertIn('atime', result)

    def test_add_sample(self):
        group = Group.objects.create(name='Группа')
        qs = Sample.objects.all()
        self.assertEqual(qs.count(), 0)
        url = resolve_url('textrank:samples')
        data = {'group': group.id, 'message': 'text'}
        response = self.client.post(url, data)
        self.assertIn(response.status_code, (302, 403), response.content)
        self.client.force_login(User.objects.create(username='user'))
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200, response.content)
        self.assertEqual(qs.count(), 1)
        newsample = qs[0]
        self.assertEqual(newsample.group, group)
        self.assertEqual(newsample.message, 'text')

    def test_change_sample(self):
        group1 = Group.objects.create(name='Группа 1')
        group2 = Group.objects.create(name='Группа 2')
        qs = Sample.objects.all()
        self.assertEqual(qs.count(), 0)
        sample = qs.create(group=group1, message='text1')
        self.assertEqual(qs.count(), 1)
        url = resolve_url('textrank:sample', id=sample.id)
        data = {'group': group2.id, 'message': 'text2'}
        response = self.client.post(url, data)
        self.assertIn(response.status_code, (302, 403), response.content)
        self.client.force_login(User.objects.create(username='user'))
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200, response.content)
        newsample = qs[0]
        self.assertEqual(newsample.group, group2)
        self.assertEqual(newsample.message, 'text2')

    def test_delete_sample(self):
        group = Group.objects.create(name='Группа')
        qs = Sample.objects.all()
        self.assertEqual(qs.count(), 0)
        sample = qs.create(group=group, message='text')
        self.assertEqual(qs.count(), 1)
        url = resolve_url('textrank:sample', id=sample.id)
        response = self.client.delete(url)
        self.assertIn(response.status_code, (302, 403), response.content)
        self.client.force_login(User.objects.create(username='user'))
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 200, response.content)
        self.assertEqual(qs.count(), 0)

    def test_add_topic(self):
        qs = Topic.objects.all()
        self.assertEqual(qs.count(), 0)
        url = resolve_url('textrank:topics')
        data = {'name': 'Раздел 1', 'code': 'code'}
        response = self.client.post(url, data)
        self.assertIn(response.status_code, (302, 403), response.content)
        self.client.force_login(User.objects.create(username='user'))
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200, response.content)
        self.assertEqual(qs.count(), 1)
        new_topic = qs[0]
        self.assertEqual(new_topic.name, 'Раздел 1')
        self.assertEqual(new_topic.code, 'code')

    def test_change_topic(self):
        qs = Topic.objects.all()
        self.assertEqual(qs.count(), 0)
        topic = qs.create(name='Раздел 1')
        self.assertEqual(qs.count(), 1)
        url = resolve_url('textrank:topic', id=topic.id)
        data = {'name': 'Раздел 2', 'code': 'code'}
        response = self.client.post(url, data)
        self.assertIn(response.status_code, (302, 403), response.content)
        self.client.force_login(User.objects.create(username='user'))
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200, response.content)
        ch_topic = qs[0]
        self.assertEqual(ch_topic.name, 'Раздел 2')
        self.assertEqual(ch_topic.code, 'code')

    def test_delete_topic(self):
        qs = Topic.objects.all()
        self.assertEqual(qs.count(), 0)
        topic = qs.create(name='Раздел 1')
        self.assertEqual(qs.count(), 1)
        url = resolve_url('textrank:topic', id=topic.id)
        response = self.client.delete(url)
        self.assertIn(response.status_code, (302, 403), response.content)
        self.client.force_login(User.objects.create(username='user'))
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 200, response.content)
        self.assertEqual(qs.count(), 1)
        self.assertEqual(qs.filter(is_active=True).count(), 0)
